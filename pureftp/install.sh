#!/usr/bin/env bash


echo 'Installing proftpd, you may be prompted for password'

sudo su
apt-get install proftpd && apt-get install proftpd-mod-mysql -y

echo 'Adding default ftp users'

groupadd -g 2001 ftpgroup
useradd -u 2001 -s /bin/false -d /bin/null -c "proftpd user" -g ftpgroup ftpuser

echo 'Overriding profptd configurations'
cp -f modules.conf /etc/proftpd/modules.conf
cp -f proftpd.conf /etc/proftpd/proftpd.conf



echo 'Installing database tables, you may be prompted for root password'
mysql -u root -p < ftp.sql


echo 'Restarting ftp server'
sudo service proftpd restart