var express = require("express");
var Rsync = require("rsync");
var watchr = require("watchr");
var mdkirp = require("mkdirp");
var config = require("config");
var Log = require("log");
var log = new Log('info');
var process = require("process");


process.env.serverType = config.get('Replica.type');
log.info("Setted process enviorement for server type "+process.env.serverType)



var storagePath = config.get("Replica.storage_path");
mdkirp(storagePath, function(err){
    if(err){
        log.error("Error creating path, reason: "+err)
        return console.log(err)
    }
    log.info("Crated storage path: "+storagePath)
})

var portConfig = config.get("Replica.port")

app = express();

app.listen(portConfig, function(err){
    if(err){
        log.error("Could not create replica server on port "+portConfig+" reason: "+err);
        return console.log(err)
    }
    log.info("Replica server started on port "+portConfig)
})


watchr.watch({
    path: storagePath,
    listeners:{
        log : function(logLevel){
            //log.info(logLevel)
        },
        error: function(err){
            log.error(err)
        },
        wathcing: function(err, watcherInstance, isWatching){
            if (err){
                log.error("watching the path " + watcherInstance.path + " failed with error", err);
            }else{
                log.info("watching the path " + watcherInstance.path + " completed");
            }
        },
        change : function(changeType,filePath,fileCurrentStat,filePreviousStat){
            if(changeType==='create'){
                var rsync = new Rsync()
                    .shell('ssh')
                    .flags('aze')
                    .source(storagePath)
                    .destination(config.get('Rsync.server_path'));

                rsync.execute(function(err, code, cmd){
                    if (err){
                        return log.error("Rsync error "+err)
                    }
                })
            }
            if (changeType==='update'){
                var rsync = new Rsync()
                    .shell('ssh')
                    .flags('azeu')
                    .source(storagePath)
                    .destination(config.get('Rsync.server_path'));

                rsync.execute(function(err, code, cmd){
                    if (err){
                        return log.error("Rsync error "+err)
                    }
                })
            }
            if (changeType==='delete'){
                var rsync = new Rsync()
                    .shell('ssh')
                    .flags('r')
                    .delete()
                    .source(storagePath)
                    .destination(config.get('Rsync.server_path'));


                rsync.execute(function(err, code, cmd){
                    if (err){
                        return log.error("Rsync error "+err)
                    }
                })
            }
        }
    }
});

