<VirtualHost {domain}>
  ServerName              "{server_name}"
  TransferLog             {log_path}
  MaxLoginAttempts        3
  RequireValidShell       no
  DefaultRoot             {path}
  User                    {user}
  Group                   {group}
  #AllowOverwrite          yes
</VirtualHost>