/**
 * Created by Linkho on 8/25/15.
 */
var Buffer = require('buffer').Buffer;

module.exports.CommandParser = function(encodedCommand) {
    var b = new Buffer(encodedCommand, 'base64');
    var command = JSON.parse(b.toString());
    return command;
}