/**
 * Created by Linkho on 8/25/15.
 */


var extend       = require('util')._extend;
var commonErrors = require('./shared_modules/exceptions.js');
var logger       = require('log4js').getLogger();

var vhosts = null;

var possibleCommands = {
    AddHost: "AddHost",
    DelHost: "DelHost",
    ListHosts: "ListHosts"
}

var options = {
    confDir: '/etc/nginx/conf.d/',
    pidLocation: '/var/run/nginx.pid',
    template: ''
}


var NginxModule = function (){};

function addHost (command, cb) {

    logger.debug("addHost" + JSON.stringify(command));
    var opts = {};
    opts.name = command.argument.domain;
    opts.config  = generateConfig(command.argument);
    vhosts.write(opts, function(err, stdout, stderr) {
        logger.debug('Error: ' + err);
        logger.debug('Stdout: ' + stdout);
        logger.debug('Stderr: ' + stderr);
        if (cb) cb(err);
    });
}

function delHost(command) {

}

function listHosts(command) {

}

function generateConfig(info) {
    /* Vhost config */
    var sconfig = options.template;
    sconfig = sconfig.replace('{domain}', info.domain);
    sconfig = sconfig.replace('{port}', info.port ? info.port : "80");
    sconfig = sconfig.replace('{path}', info.path);
    return sconfig;
}

NginxModule.prototype.handle = function(command, cb) {

    try {
        if (!command) throw commonErrors.NullArgument;
        if (command.command != 'vhost') throw  commonErrors.UknownArgument;

        switch (command.argument.subcommand) {
            case possibleCommands.AddHost:
                addHost(command, cb);
                break;
            case possibleCommands.DelHost:
                delHost(command, cb);
                break;
            case possibleCommands.ListHosts:
                listHosts(command, cb);
                break;

            default :
                throw commonErrors.UknownArgument;
        }
    } catch (ex) {
        cb(err = ex);
    }
};

NginxModule.prototype.config = function(opts) {
    options = extend(options, opts);
    vhosts = require('nginx-vhosts')(options);
};

module.exports = new NginxModule();