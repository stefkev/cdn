/**
 * Created by Linkho on 8/25/15.
 */

var CommonErrors =  {
    NullArgument : "NullArgument",
    UknownArgument : 'UknownArgument',
    UknownError: "UknownError"
};

module.exports = CommonErrors;