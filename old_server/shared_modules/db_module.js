/**
 * Created by Linkho on 9/5/15.
 */

var mysql        = require('mysql');
var extend       = require('util')._extend;
var commonErrors = require('./exceptions.js');
var logger       = require('log4js').getLogger();

var MySqlModule = function () {};

var opts = {
    UserTable: 'ftpuser',
    GroupTable: 'ftpgroup',
    QuotaTable: 'ftpquotalimits'
}

var config = {
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'push_zone'
};

function getConnection(cb) {
    var connection = mysql.createConnection(config);
    connection.connect(function(err){
        if(!err) {
            cb(connection);
        }
        else {
            cb(null, err);
        }
    });
}

MySqlModule.prototype.getGroup = function(condition, cb) {
    if (!condition) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('SELECT * from ' + opts.GroupTable, condition, function(err, rows, fields) {
                connection.end();
                if (!err) {
                    cb(rows[0]);
                } else {
                    logger.error(err);
                    cb(null, err);
                }

            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });

}

MySqlModule.prototype.getGroupMembers = function(gid, cb) {
    if (!gid) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('SELECT * from ?? WHERE ?', [opts.UserTable, {gid: gid}], function(err, rows, fields) {
                connection.end();
                if (!err) {
                    cb(rows[0]);
                } else {
                    logger.error(err);
                    cb(null, err);
                }

            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });

}

MySqlModule.prototype.addGroup = function(group, cb) {
    if (!group) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('INSERT INTO ?? SET ?', [opts.GroupTable, group], function(err, result) {
                connection.end();
                if (!err) {
                    logger.info('Group Inserted Id: ', result.insertId);
                    cb(result);
                } else {
                    logger.error(new Error('Error while performing Query.'));
                    cb(null, err);
                }
            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });

}

MySqlModule.prototype.removeGroup = function(condition, cb) {
    if (!condition) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('DELETE FROM ?? WHERE ?', [opts.GroupTable, condition], function(err, result) {
                connection.end();
                if (!err) {
                    logger.info('Group affectedRows: ', result.affectedRows);
                    cb(result);
                } else {
                    logger.error(new Error('Error while performing Query.'));
                    cb(null, err);
                }
            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });
}

MySqlModule.prototype.addUser = function(user, cb) {
    if (!user) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
       if(!err) {
           connection.query('INSERT INTO ?? SET ?', [opts.UserTable, user], function(err, result) {
               connection.end();
               if (!err) {
                   logger.info('User Inserted Id: ', result.insertId);
                   cb(result);
               } else {
                   logger.error(new Error('Error while performing Query.'));
                   cb(null, err);
               }
           });

       } else {
           logger.error(err);
           cb(null, err);
       }
    });

}

MySqlModule.prototype.removeUser = function(condition, cb) {
    if (!condition) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('DELETE FROM ?? WHERE ?', [opts.UserTable, condition], function(err, result) {
                connection.end();
                if (!err) {
                    logger.info('Group affectedRows: ', result.affectedRows);
                    cb(result);
                } else {
                    logger.error(new Error('Error while performing Query.'));
                    cb(null, err);
                }
            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });
}

MySqlModule.prototype.getUser = function(condition, cb) {
    if (!condition) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('SELECT * from ?? WHERE ?', [opts.UserTable, condition], function(err, rows, fields) {
                connection.end();
                if (!err) {
                    cb(rows[0]);
                } else {
                    logger.error(err);
                    cb(null, err);
                }

            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });

}

MySqlModule.prototype.changeUserPassword = function(username, password, cb) {
    if (!username || password == undefined || password.length == 0 || cb == undefined) {
        if (!cb)
            cb(new Error(commonErrors.NullArgument));
        else
            throw new Error(commonErrors.NullArgument);
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('UPDATE ?? SET ? WHERE ?', [opts.UserTable, {passwd: password}, {userid: username}], function(err, result) {
                connection.end();
                if (!err) {
                    logger.info('User affectedRows: ', result.affectedRows);
                    cb(result);
                } else {
                    logger.error(new Error('Error while performing Query.'));
                    cb(null, err);
                }
            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });
}

MySqlModule.prototype.getUserQuota = function(username, cb) {
    if (!username) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    getConnection(function(connection,err) {
        if(!err) {
            connection.query('SELECT * from ?? WHERE ?', [opts.QuotaTable, {name: username}], function(err, rows, fields) {
                connection.end();
                if (!err) {
                    cb(rows[0]);
                } else {
                    logger.error(err);
                    cb(null, err);
                }

            });

        } else {
            logger.error(err);
            cb(null, err);
        }
    });
}

MySqlModule.prototype.setUserQuota = function (username, MBInLimit, MBOutLimit, cb) {
    if (!username || MBInLimit == undefined || MBOutLimit == undefined || cb == undefined) {
        cb(new Error(commonErrors.NullArgument));
        return;
    }

    this.getUserQuota(username,function(result, err){
        if(err) {
            cb(null, err);
            return;
        }
debugger;
        getConnection(function(connection, err) {
            debugger;
            if(!err) {
                if(result == undefined) {
                    /* Insert into quota limit table */
                    connection.query('INSERT INTO ?? SET ?', [opts.QuotaTable, {
                        name: username,
                        bytes_in_avail: MBInLimit * 1024 * 1024,
                        bytes_out_avail: MBOutLimit * 1024 * 1024
                    }], function (err, result) {
                        connection.end();
                        if (!err) {
                            logger.info('QuotaLimit Inserted Id: ', result.insertId);
                            cb(result);
                        } else {
                            logger.error(new Error('Error while performing Query.'));
                            cb(null, err);
                        }
                    });
                } else {
                    /* Update existing value in quota limit table */
                    connection.query('UPDATE ?? SET ? WHERE ?', [opts.QuotaTable, {
                        bytes_in_avail: MBInLimit * 1024 * 1024,
                        bytes_out_avail: MBOutLimit * 1024 * 1024
                    }, username]
                        , function (err, result) {
                            connection.end();
                            if (!err) {
                                logger.info('QuotaLimit affected rows: ', result.affectedRows);
                                cb(result);
                            } else {
                                logger.error(new Error('Error while performing Query.'));
                                cb(null, err);
                            }
                        }
                    );
                }

            } else {
                logger.error(err);
                cb(null, err);
            }
        });
    });


}

MySqlModule.prototype.config = function(opts) {
    config = extend(config, opts);
};

module.exports = new MySqlModule();