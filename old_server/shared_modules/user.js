/**
 * Created by Linkho on 9/7/15.
 */

var extend        = require('util')._extend;
var commonErrors  = require('./exceptions.js');
var logger        = require('log4js').getLogger();
var path          = require('path');
var useradd       = require('useradd');
var child_process = require('child_process');


var UserModule = function () {};

var opts = {
    home_dir: '/home/sites',
    default_shell: '/bin/nologin'
}

UserModule.prototype.addUser = function(username, gid, domain, callback) {
    if(!username || !domain || !gid) {
        callback(new Error(commonErrors.NullArgument));
        return;
    }

    this.isGroupExist(gid, function(result, err) {
        if(!err) {
            var home_dir = this.getHomeDir(domain);

            /* Check whether group exist */
            if(!result) {
                this.addGroup(gid, function(err) {
                    if(err) {
                        logger.error(err);
                        callback(err);
                        return;
                    }
                    useradd(
                        {
                            login: username,
                            gid: gid,
                            home: home_dir,
                            shell: opts.default_shell
                        }, function (err) {
                            if (!err)
                                callback();
                            else {
                                logger.error(err);
                                callback(err);
                            }
                        });
                });
            } else {
                useradd(
                    {
                        login: username,
                        gid: gid,
                        home: home_dir,
                        shell: opts.default_shell
                    }, function (err) {
                        if (!err)
                            callback();
                        else {
                            logger.error(err);
                            callback(err);
                        }
                    });
            }

        } else {
            logger.error(err);
            callback(err);
        }
    });
}

UserModule.prototype.removeUser = function(username, callback) {
    if(!username) {
        callback(new Error(commonErrors.NullArgument));
        return;
    }
    callback(new Error('Not Implemented yet'));
}

UserModule.prototype.addGroup = function(groupname, callback) {
    if(!groupname) {
        callback(new Error(commonErrors.NullArgument));
        return;
    }
    var child = child_process.spawn('/bin/bash', '-c groupadd ' + groupname);
    child.on('exit', function (code) {
        var err;
        logger.info(child.stdout);
        if (code) {
            err = new Error('Invalid exit code: ' + code);
            err.code = code;
            return callback(err);
        }
        callback();
    });

}

UserModule.prototype.isGroupExist = function(gid, callback) {
    var bash = 'grep';
    var args = [];
    args.push(gid + ':');
    args.push('/etc/group');
    var child = child_process.spawn(bash, args);
    var output = '';
    child.stdout.on('data', function(data){
        output += data;
    });
    child.on('exit', function (code) {
        var err;
        if (code) {
            err = new Error('Invalid exit code: ' + code);
            err.code = code;
            return callback(null, err);
        }
        callback(output.indexOf(gid) != -1);
    });

}

UserModule.prototype.getHomeDir = function (domain) {
    return path.join(opts.home_dir, domain);
}

UserModule.prototype.config = function (options) {
    opts = extend(opts, options);
}

module.exports = new UserModule();