/**
 * Created by Linkho on 9/7/15.
 */

var user = require('../shared_modules/user.js');

module.exports = {
    setUp: function (callback) {
        callback();
    },
    tearDown: function (callback) {
        // clean up
        callback();
    },
    addUser: function (test) {
        test.done();
    },
    removeUser: function(test) {
        test.done();
    },
    isGroupExist: function(test) {
        user.isGroupExist('root', function(result, err) {
            test.equal(err, null);
            test.equal(result, true);
            test.done();
        });;
    },
    getHomeDir: function(test) {
        user.config({
            home_dir: '/home/sites'
        });
        var home_dir = user.getHomeDir('test.com');
        test.equal(home_dir, '/home/sites/test.com');
        test.done();
    }
};