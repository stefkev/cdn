/**
 * Created by Linkho on 9/5/15.
 */

var db = require('./../shared_modules/db_module.js');

module.exports = {
    setUp: function (callback) {
        callback();
    },
    tearDown: function (callback) {
        // clean up
        callback();
    },
    addGroup: function (test) {
        db.addGroup({
            groupname: 'ti',
            gid: 5500,
            members: 'ti'
        },function(result, err){
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    },
    getGroup: function (test) {
        db.getGroup({
            groupname: 'ti'
        }, function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    },
    removeGroup: function (test) {
        db.removeGroup({
            groupname: 'ti'
        }, function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    },
    addUser: function (test) {
        db.addUser({
            userid: 'ti',
            gid: 5500,
            uid: 5500,
            passwd: 'test',
            homedir: '/home/ti'
        }, function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    },
    getUser: function (test) {
        db.getUser({
            userid: 'ti'
        }, function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    },
    changePasswd: function(test) {

        db.changeUserPassword('ti', 'npass', function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            db.getUser({
                userid: 'ti'
            }, function(result, err) {
                test.notEqual(result, null);
                test.equal('npass', result.passwd);
                test.equal(err, null);
                test.done();
            });
        });
    },
    setUserQuota: function(test) {
        db.setUserQuota('ti',1,0, function(result, err) {
            test.notEqual(result, null);
            test.equal(err, null);
            db.getUserQuota('ti',function(result, err){
                test.notEqual(result, null);
                if (result) {
                    test.equal(result.bytes_in_avail, 1 * 1024 * 1024);
                    test.equal(result.bytes_out_avail, 0);
                }
                test.equal(err, null);
                test.done();
            });
        });

    },
    removeUser: function (test) {
        db.removeUser({
            userid: 'ti'
        }, function(result, err){
            test.notEqual(result, null);
            test.equal(err, null);
            test.done();
        });
    }
};
