/**
 * Created by Linkho on 9/7/15.
 */

var ftpd = require('../ftpd.js');

module.exports = {
    setUp: function (callback) {
        callback();
    },
    tearDown: function (callback) {
        // clean up
        callback();
    },
    reloadFtpd: function (test) {
        ftpd.reloadFtpd(function(err) {
            test.equal(err, null);
        });
        test.done();
    }
};