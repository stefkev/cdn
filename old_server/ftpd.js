/**
 * Created by linkho on 8/29/15.
 */

var extend        = require('util')._extend;
var fs            = require('fs');
var commonErrors  = require('./shared_modules/exceptions.js');
var logger        = require('log4js').getLogger();
var child_process = require('child_process');


var possibleCommands = {
    AddHost: "AddHost",
    DelHost: "DelHost",
    ListHosts: "ListHosts"
}

var options = {
    confDir: '/etc/proftpd/conf.d/',
    template: '',
    serviceName: 'proftpd'
}


var FtpModule = function (){};

function addHost (command, cb) {
    try {
        logger.debug('Ftp: [addHost] [' + JSON.stringify(command) + ']');
        var opts = {};
        opts.name = command.argument.domain;
        opts.config = generateConfig(command.argument);
        var fileName = getFileName(command);
        writeConfig(fileName, opts.config);
        cb();
    } catch (err) {
        logger.error(err);
        cb(err);
    }
}

function delHost(command) {

}

function listHosts(command) {

}

function generateConfig(info) {
    /* Vhost config */
    var sconfig = options.template;
    sconfig = sconfig.replace('{domain}', info.domain);
    sconfig = sconfig.replace('{port}', info.port ? info.port : "80");
    sconfig = sconfig.replace('{path}', info.path);
    return sconfig;
}

function writeConfig(fileName, config) {
    fs.writeFileSync(fileName, config);
}

FtpModule.prototype.reloadFtpd = function(callback) {

    var bash = '/bin/bash';
    var args = [];
    args.push('-c');
    args.push('service ' + options.serviceName + ' reload');
    var child = child_process.spawn(bash, args);
    child.on('exit', function (code) {
        var err;

        if (code) {
            err = new Error('Invalid exit code: ' + code);
            err.code = code;
            return callback(err);
        }
        callback();
    });

}

FtpModule.prototype.handle = function(command, cb) {

    try {
        if (!command) throw commonErrors.NullArgument;
        if (command.command != 'ftp') throw  commonErrors.UknownArgument;

        switch (command.argument.subcommand) {
            case possibleCommands.AddHost:
                addHost(command, cb);
                break;
            case possibleCommands.DelHost:
                delHost(command, cb);
                break;
            case possibleCommands.ListHosts:
                listHosts(command, cb);
                break;

            default :
                throw commonErrors.UknownArgument;
        }
    } catch (ex) {
        cb(err = ex);
    }
};

FtpModule.prototype.config = function(opts) {
    options = extend(options, opts);
    vhosts = require('nginx-vhosts')(options);
};

module.exports = new FtpModule();