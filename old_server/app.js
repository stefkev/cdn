/**
 * Created by Linkho on 8/25/15.
 */

var fs     = require('fs');
var parser = require('./CommandParser.js');
var nginx  = require('./nginx.js');
var ftpd  = require('./ftpd.js');
var logger = require('./logger.js');

var config = loadConfig();
config.templates = loadTemplates();

nginx.config({
    confDir: config.nginx_config_dir,
    pidLocation: config.nginx_pid_file,
    template: config.templates.nginx
});

ftpd.config({
    confDir: config.ftp_config_dir,
    template: config.templates.ftp
});


if (false && process.argv.length != 3 ) {
    logger.error('Bad arguments');
    process.exit(1);
}


var encodedCommand = process.argv[3];
/* Test command */
encodedCommand = "ew0KICAgICJjb21tYW5kIiA6ICJ2aG9zdCIsDQogICAgImFyZ3VtZW50Ijogew0KICAgICAgICAic3ViY29tbWFuZCIgOiAiQWRkSG9zdCIsDQogICAgICAgICJkb21haW4iIDogInRlc3QuY29tIiwNCiAgICAgICAgInBvcnQiIDogIjgwIg0KICAgIH0NCn0=";

var command = parser.CommandParser(encodedCommand);

command = {
    "command" : "vhost",
    "argument": {
        "subcommand" : "AddHost",
        "domain" : "test.com",
        "port" : "80",
        "path" : '/usr/local/nginx_home/'
    }
};

/* Test command */

/* Main */

switch (command.command) {
    case 'vhost':
        debugger;
        nginx.handle(command, callback);
        break;
    case 'ftp':
        ftpd.handle(command, callback);
        break;

    default :
        logger.error('Unknown command has received');
        exit();
}


/* Global call back */
function callback(err) {
    if (err)
        logger.error('handle : ' + err);
    exit();
}

function exit() {
    logger.debug('exit');
    process.exit();
}

function loadTemplates() {
    debugger;
    var templates = {
        "nginx" : "",
        "ftp" : ""
    };
    var data = fs.readFileSync('./config_templates/nginx.txt');
    templates.nginx = data.toString();
    data = fs.readFileSync('./config_templates/ftpd.txt');
    templates.ftp = data.toString();
    return templates;
}


/* Load configs */
function loadConfig() {
    logger.debug("Loading configs");
    var def = {
        "nginx_config_dir" : "/etc/nginx/conf.d",
        "nginx_pid_file" : "/var/run/nginx.pid'",
        "home_dir" : "/usr/local/nginx_home/",
        "ftp_config_dir" : "/etc/proftpd/conf.d/"
    };

    if (fs.existsSync('./config.json')) {
        var data = fs.readFileSync('./config.json');
        if (!data) return def;
        return JSON.parse(data);
    } else {
        return def;
    }
}

