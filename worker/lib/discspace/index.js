var disk = require('diskusage');
var bytes = require('bytes');
var util = require('util')


module.exports = disk.check('/', function(err, info) {
    if (err){
        return console.log(util.inspect({error: err}))
    }
    var data = {
        available: bytes(info.available),
        free: bytes(info.free),
        total: bytes(info.total)
    }
    console.log(util.inspect(data))
});

