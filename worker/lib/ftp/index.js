var bytes = require("bytes")
var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'ftp'
});



function Ftp(){
    Ftp.prototype.createUser = function(username, password, quotalimit){
        if(!username){
            return throw 'Missing username parameter';
        }
        if(!password){
            return throw 'Missing password parameter';
        }
        if(!quotalimit){
            quotalimit = 15;
        }
        var bytequota = quotalimit*1024;
        connection.query('INSERT INTO ftpuser SET ?', {
            userid: username,
            passwd: password,
            uid: 2001,
            gid: 2001,
            homedir: '/home/'+username,
            shell: '/sbin/nologin',
            count: 0,
            accessed: '',
            modified: ''
        }, function(err){
            if (err){
                return throw err;
            }
        })

        connection.query('INSERT INTO ftpquotalimits SET ?',{
            id: username,
            quota_type: "user",
            per_session: "true",
            limit_type: "hard",
            bytes_in_available: bytequota,
            bytes_xfer_avail: 0,
            files_in_avail: 0,
            files_out_avail : 0,
            files_xfer_avail : 0
        }, function(err){
            if(err){
                return throw err;
            }
        })

        return 0;
    }

    Ftp.prototype.deleteUser = function(username){
        if(!username){
            return throw 'Missing username parameter';
        }
        connection.query("DELETE FROM ftpquotalimits WHERE name="+username, function(err){
            if(err){
                return throw err;
            }
        })
        connection.query("DELETE FROM ftpuser WHERE userid="+username, function(err){
            if(err){
                return throw err;
            }
        })
        return 0;
    }
    Ftp.prototype.updatePassword = function(username, newPassword){
        if(!username){
            return throw 'No username parameter';
        }
        connection.query('SELECT username FROM ftpuser WHERE :username',{username: username}, function(err, row){
            if (!row[0].username){
                return throw 'No matching username'
            }
        })
        connection.query("UPDATE ftpuser WHERE userid = :username SET passwd = :password", {username: username, password: newPassword})
    }

    Ftp.prototype.updateUserQuota = function(username, newQuota){
        if(!username){
            return throw 'No username parameter;'
        }
        if(!newQuota){
            return throw 'No quota parameter'
        }
        var byteQuota = newQuota*1024;
        connection.query("UPDATE ftpquotalimits WHERE name = :username SET bytes_in_available = :newQuota", {username: username,
            newQuota: byteQuota});

        return 0;
    }
    Ftp.prototype.getAllTransferedData = function(){
        connection.query("SELECT SUM(bytes_xfer_used) AS totalBytes FROM ftpquotatallies", function(err, res){
            return bytes(res[0].totalBytes);

        })
    }
}



module.exports = Ftp;