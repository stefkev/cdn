var util = require("util");
var EventEmitter = require('events');
var exec = require('child_process').exec;
var nginxStatusEmmiter = new EventEmitter();

function Nginx(){
    EventEmitter.call(this);
}

util.inherits(Nginx, EventEmitter);

var vhosts = require("nginx-vhosts")(function running(isRunning){
    if (isRunning === true){
        nginxStatusEmmiter.emit("status", true)
        console.log("Nginx is running");
    }else{
        nginxStatusEmmiter.emit("status", true)
        console.log("Nginx is not running")
    }
});

Nginx.prototype.createVhost = function(nema, port, domain, cb){
    vhosts.write({
        name: name,
        port: port,
        domain: domain
    }, function(err, stdout, stderr){
        if(err){
            return cb('error', {err:err, stdout: stdout, stderr: stderr})
        }
        return cb('data', {err: null, stdout: stdout, stderr:stderr})
    })
}



Nginx.prototype.removeVhosts = function(name, cb){
    vhosts.remove(name, function(err, stdout, stderr){
        if(err){
            return this.emit('err', {err:err, stdout:stdout, stderr:stderr})
        }
        return this.emit('data', {err: null, stdout: stdout, stderr:stderr})
    })
}

Nginx.prototype.status = function(){
    exec('goaccess -f access.log -a -d -o json > report.json', function(err, stdout, stderr){
        return console.log(util.inspect(stdout))
    })
}

module.exports = Nginx;
