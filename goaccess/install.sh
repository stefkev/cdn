#!/usr/bin/env bash


wget http://tar.goaccess.io/goaccess-0.9.6.tar.gz
tar -xzvf goaccess-0.9.6.tar.gz
cd goaccess-0.9.6/
./configure --enable-geoip --enable-utf8
make
make install