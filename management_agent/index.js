
var express = require("express");

var Log = require("log")
var log = new Log('info');
var config = require("config");
var broker = require("./lib/celery");
var bodyParser = require("body-parser");
var broker = require("./lib/celery")

var port = config.get('server.port')

var app = express();
app.set('port', process.env.PORT || port);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/ftp', require("./routes/ftp"));
app.use('/status', require("./routes/status"));
app.use('/vhost', require("./routes/vhost"))


app.listen(port, function(err){
    if (err){
        return log.error('Management agent failed to start on port '+port+' reason:\n'+err)
    }
    log.info('Management agent started on port '+port)
})