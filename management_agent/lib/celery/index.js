
var Log = require("log");
var log = new Log('info');
var celery = require("node-celery");
var config = require("config");


var broker_url = config.get('celery.broker_url');
var backend_url = config.get('celery.backend_url')


var createTask = function(taskName, queue, args, cb) {
    log.info('queue name '+queue)
    var client = celery.createClient({
        CELERY_BROKER_URL: broker_url,
        CELERY_RESULT_BACKEND: 'amqp',
        CELERY_TASK_RESULT_EXPIRES : 3600,
        CELERY_TASK_RESULT_DURABLE: false,
        CELERY_ACCEPT_CONTENT : 'json',
        CELERY_TASK_SERIALIZER : 'json' ,
        CELERY_RESULT_SERIALIZER : 'json',
        CELERY_DEFAULT_QUEUE : queue


    });

    var task = client.createTask('tasks.dispatchTask',{expires: new Date(Date.now() + 5 * 60 * 1000), queue:queue });

    client.on('error', function(err) {
        log.error(err)
    });

    client.on('connect', function() {
        var stringifiedArgs = JSON.stringify(args)
        var result = task.call([taskName, stringifiedArgs]);
        result.on('ready', function(message) {
            client.end();
            client.broker.destroy();
            return cb({data:message.result})
        });
    });
}

module.exports.createTask = createTask;