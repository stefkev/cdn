var express = require("express");
var router = express.Router();
var broker = require("../lib/celery")

router.all('/', function(req, res, next){
    if(!req.body.queue){
        return res.jsonp({
            err: "Missing queue name"
        })
        return next();
    }
})

router.get('/space', function(req, res){
    var queue = req.body.queue;
    broker.createTask('getSpaceStatus',null, queue, function(data){
        return res.jsonp({data:data})
    })
})

router.get('/traffic', function(req, res){
    var queue = req.body.queue;
    broker.createTask('getTraffic', null, queue, function(data){
        return res.jsonp({data:data})
    })
})

module.exports = router;