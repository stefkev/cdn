var express = require("express");
var router = express.Router();

var broker = require("../lib/celery");

router.all('/', function(req, res, next){
    if (!req.body.queue){
        return res.json({
            message: "Queue not provided"
        })
    }
    return next();
})

router.post('/create/user', function(req, res){
    var queue = req.body.queue;
    var taskName = req.body.taskName;
    var args = {}

    if(req.body.username){
        args.username = req.body.username
    }
    else{
        return res.jsonp({
            err: 'Missing username'
        })
    }
    if(req.body.password){
        args.password = req.body.password
    }
    else{
        return res.jsonp({
            err: 'Missing password'
        })
    }
    broker.createTask(createFtpUser, args, queue, function(data){
        return res.json(data)
    })
})
router.post('/update/user', function(req, res){
    var queue = req.body.queue;
    var taskName = req.body.taskName;
    var args = {}

    if(req.body.username){
        args.username = req.body.username;
    }else{
        return res.jsonp({err: "Username not provided"})
    }

    if (req.body.password){
        args.password = req.body.password;
    }else{
        return res.json({err: 'Missing password'})
    }
    broker.createTask('updateFtpUser', args, queue, function(data){
        return res.json(data)
    })
})

router.get('/list/user', function(req, res){
    var queue = req.body.queue;
    var taskName = req.body.taskName;
    var args = {}
    broker.createTask('listFtpUser', args, queue, function(data){
        return res.json(data)
    })
})

module.exports = router;



