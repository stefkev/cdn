var express = require("express");
var router = express.Router();
var broker = require('../lib/celery');
var Log = require("log")
var log = new Log('info');


router.all('/', function(req, res, next){
    if(!req.body.queue){
        return res.jsonp({
            err: "Missing queue name"
        })
        return next();
    }
})

router.post('/create', function(req, res){
    log.info(req.body)
    var queue = req.body.queue;
    var hostName = req.body.hostname;
    var args = {};

    if(hostName){
        args.hostName = hostName
    }else{
        return res.jsonp({
            err: "Invalid hostname"
        })
    }

    broker.createTask('createVhost',queue, args, function(data){
        res.jsonp(data)
    })
})

router.post('/update', function(req, res){
    var queue = req.body.queue;
    var hostName = req.body.hostname;
    var args = {};

    if(hostName){
        args.hostName = hostName
    }else{
        return res.jsonp({
            err: "Invalid hostname"
        })
    }


    broker.createTask('updateVhost', args, queue, function(data){
        return res.jsonp({data:data})
    })
})

router.post('/delete', function(req, res){
    var queue = req.body.queue;
    var hostName = req.body.hostname;
    var args = {};

    if(hostName){
        args.hostName = hostName
    }else{
        return res.jsonp({
            err: "Invalid hostname"
        })
    }


    broker.createTask('deleteVhost', args, queue, function(data){
        return res.jsonp({data:data})
    })
})

module.exports = router;