var express = require("express");
var Log = require("log")
var log = new Log('info')

var app = express();


app.set('port', process.env.PORT || 3000);

app.use(express.static('/home/'))

app.listen(app.get('port'), function(err){
    if(err){
        return log.error('Push server failed to start at port '+app.get('port'))
    }
    log.info("Push server started at port "+app.get('port'))
})